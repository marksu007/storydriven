// type gulp watch in the terminal from the postmedia-wordpress directory

module.exports = {
  main: {
    src: [
      'scss/main.scss',
    ],
    watch: [
      'scss/**/*.scss',
    ],
    dest: 'css',
    minimize: true,
    map: true,
  },
};
