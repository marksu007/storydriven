## Storydriven Developer Test

* Set up a REST API that provides text and image urls for this single page - https://www.maracielo.com/residences/.
* Set up a site/web app that calls the Rest API and retrieves the page data used reconstruct the look and feel of the same page: https://www.maracielo.com/residences/.
* Image assets are supplied in repository
* Fonts can be included through google fonts '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700'. ('Lato', sans-serif)
* Build the page prioritizing component functionality and reuse, structure and styling (colors and layout) of the page, the header nav and footer sections can be screenshots.
* Ignore form functionality.
* Ignore AgentShield script functionality.
* Integrate with as many modern development features as possible (e.g. Live reloading, linting, script bundling + minification, image compression, font loading, SASS, ES6 javascript).
* This site uses Bootstrap 4 but you may use any library if need be.
* Both the REST API and site/web app should be able to run as localhost servers and supply minimal documentation to start both.
* Complete as much as possible in 2 hours.
* Commit all code as well as Readme file instructions for how to serve both the REST API and site/web app to a github/bitbucket repository.
* Feel free to ask any questions if stuck or anything is missing - aedan@storydriven.com.


Sorry, I didn't get to style and finish much


Folder Structure.



Plugins folder is a plugin create a custom post in wordpress use as json api 

React folder will contain the web app which access the api and render the template 

html folder will be where the main document goes, react app will be plugged in to this page

1. API set up, Wordpress is used as the cms/api server, this utilizes wordpress's wp json api. 
Install and activate the plugin to a wordpress installation.
It will create a new post type in the admin where you can add and edit "SDElement"
please add couple new  "SDElement", 3 fields used in this, 
"Add title" // this is planned to be used as the text title overlay on top of the image
"Headings,Paragraph" // these blocks will become the content of the post which will be used as the text belovw the image, including headings
"Featured Image" // this will be used as the image

Once couple posts added, navigate to /wp-json/wp/v2/sd-element, you should see a json similiar to https://www.maracielo.com/wp-json/wp/v2/posts

2. react app, api url is hard coded at this moment, updated to the url from the api above, and save.
navigate the root of react folder, type "npm install" in termimal, it should install the all the modules used
"npm start"  should bring up the react app under  localhost:3000 for testing and 
"npm react-scripts build" will generate a build folder with bundled script and a index.html page in that folder

3. html folder, this should be all the external nav and html be, and the react app should be intergrated into the rest of the html page by copying the react portion from  above build folder/index.html

TO DO LIST
API portion(1) is basicaly functioning, need more function 
React app part(2) is loading images and texts,  need styling, 
Html portion only has the some structure set up, need styling and add missing elements

Carousel need set up , could be another react app and a different wordpress api end points