import axios from 'axios';
const instance = axios.create({
    baseURL: 'http://localhost:8000/wp-json/wp/v2/sd-element'
});
export default instance;