import React, {Component} from 'react'
import axios from './axios'

export default class Items extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Items: []
        };
    }
    getData() {
        axios
            .get(`/`, {})
            .then(res => {
                let data = res.data.map((item) => {
                    return(
                        <div key={item.id}>
                            <div class="img-max">
                                <h6>{item.title.rendered}</h6>
                                <img src={item.fimg_url} alt={item.title.rendered}  title=""/>
                            </div>
                            <div class="bg-brand-dark" dangerouslySetInnerHTML={{ __html:item.content.rendered}}>
                            </div>
                        </div>
                    )
                })

                this.setState({pictures:data});
               console.log(data)

            })
            .catch((error) => {
                console.log(error)
            })

    }
    componentDidMount(){
        this.getData()
    }
    render() {

        return (
            <div>
                {this.state.pictures}
            </div>
        )
    }
}