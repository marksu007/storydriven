<?php
/**
 * Story Driven API plugin
 *
 * @since             1.0.0
 * @package           storydriven_plugin
 *
 * @wordpress-plugin
 * Plugin Name:       Story Driven API plugin
 * Plugin URI:
 * Description:       Create custome post type for API
 * Version:           1.0.0
 * Text Domain:       storydriven-plugin
 */

namespace StoryDriven_Plugin;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
  die;
}


class SD_Element {

  const PLUGIN_NAME = 'storydriven-plugin';

  const POST_TYPE_SLUG = 'sd-element';


  const TAXONOMY_SLUG = 'sd-element-category';

  /**
   * Register custom post type
   *
   * @since 1.0.0
   */
  public function register_post_type() {
  
    $args = array(
        'label'              => esc_html( 'SDElement', 'storydriven-plugin' ),
        'public'             => true,
        'menu_position'      => 47,
        'menu_icon'          => 'dashicons-book',
        'supports'           => array( 'title', 'editor', 'revisions', 'thumbnail' ),
        'has_archive'        => false,
        'show_in_rest'       => true,
        'publicly_queryable' => false,
    );

    register_post_type( self::POST_TYPE_SLUG, $args );
  }

  public function register_rest_images(){
    register_rest_field( array('sd-element'),
        'fimg_url',
        array(
            'get_callback'    => array( $this, 'get_rest_featured_image'),
            'update_callback' => null,
            'schema'          => null,
        )
    );
    }
    public function get_rest_featured_image( $object, $field_name, $request ) {
        if( $object['featured_media'] ){
            $img = wp_get_attachment_image_src( $object['featured_media'], 'app-thumb' );
            return $img[0];
        }
        return false;
    }
}

$sdelement = new SD_Element();

add_action( 'init', [ $sdelement, 'register_post_type' ] );
add_action('rest_api_init', [ $sdelement, 'register_rest_images' ] );